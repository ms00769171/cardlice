// 要新增網頁要在這
// step1. import
// step2.加入path進入點,name,component輸出
// step3.要切頁面<router-link class= "" :to="'path'">名字隨便打</router-link> 

import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import HelloWorld2 from '@/components/HelloWorld2'
import chat from '@/components/chat'
import Sidebar from '@/components/Sidebar'
import GameTable from '@/components/GameTable'
import Backpack from '@/components/Backpack'
import Shop from '@/components/Shop'
import Factory from '@/components/Factory'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/2',
      name: 'HelloWorld2',
      component: HelloWorld2
    },
    {
      path: '/chat',
      name: 'chat',
      component: chat
    },
    {
      path: '/Sidebar',
      name: 'Sidebar',
      component: Sidebar
    },
    {
      path: '/gametable',
      name: 'GameTable',
      component: GameTable
    },
    {
      path: '/Backpack',
      name: 'Backpack',
      component: Backpack
    },
    {
      path: '/Shop',
      name: 'Shop',
      component: Shop
    },
    {
      path: '/Factory',
      name: 'Factory',
      component: Factory
    },
  ]
})
